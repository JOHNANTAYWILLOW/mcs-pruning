/*
 * PSO_Pruner.cpp
 *
 *  Created on: Feb 25, 2011
 *      Author: rgreen
 */

#include "PSO_Sampler.h"

#include <iostream>
#include <iomanip>
using namespace std;

PSO_Sampler::PSO_Sampler(int _Np, int _Nd, int _Nt) : Sampler() {

	Np = _Np;
	Nd = _Nd;
	Nt = _Nt;

	W  = 2.95;
	C1 = 3.05;
	C2 = 2.05;

	epsilon = pow(10.0,-7);
}

PSO_Sampler::~PSO_Sampler(){

}

void PSO_Sampler::initPopulation(MTRand& mt) {
	swarm.clear();
	gBestValue = 0;

	for (int i = 0; i < Np; i++) {
		binaryParticle p;
		for (int j = 0; j < (int) gens.size(); j++) {
			p.pos.push_back((mt.rand() < gens[j].getOutageRate() ? 0 : 1));
			p.vel.push_back(gens[j].getOutageRate());
			p.pBest.push_back(0);
		}
		for (int j = 0; j < (int) lines.size(); j++) {
			p.pos.push_back((mt.rand() < lines[j].getOutageRate() ? 0 : 1));
			p.vel.push_back(lines[j].getOutageRate());
			p.pBest.push_back(0);
		}
		#ifdef _MSC_VER
			p.pBestValue = -std::numeric_limits<double>::infinity();
		#else
			p.pBestValue = -INFINITY;
		#endif
		
		swarm.push_back(p);
	}
}

void PSO_Sampler::evaluateFitness(){
	bool failed;
	long double sol, stateProb;
	double totalCap, excess;
	std::string curSolution;
	std::vector<double> vCurSolution;

    if(useLines){ vCurSolution.resize(gens.size() + lines.size() + 2, 0);}
    else        { vCurSolution.resize(gens.size() + 2, 0);}

	for(unsigned int i=0; i<swarm.size(); i++){

		stateProb = 1; totalCap = 0;
		curSolution = "";
		genCount = 0; lineCount = 0;

		for(unsigned int j=0; j<gens.size(); j++){
			vCurSolution[j] = swarm[i].pos[j];
			if(vCurSolution[j] == 1){
				totalCap    += gens[j].getPG()/100;
				stateProb   *= (1-gens[j].getOutageRate());
                curSolution += "1";
			}else{
                curSolution += "0";
				stateProb *= gens[j].getOutageRate();
				genCount++;
			}
		}
		avgGenCount += genCount;
		genOutageCounts[genCount]++;

		if(useLines && lines.size() > 0){
			for(unsigned int j=0; j<lines.size(); j++){
				vCurSolution[j+gens.size()] = swarm[i].pos[j+gens.size()];

				if(vCurSolution[j] == 1){
					vCurSolution[j+gens.size()] = 1;
					stateProb *= (1-lines[j].getOutageRate()*lineAdjustment);
					curSolution += "1";
				}else{
					curSolution += "0";
					vCurSolution[j+gens.size()] = 0;
					stateProb *= (lines[j].getOutageRate()*lineAdjustment);
					lineCount++;
				}
			}
			avgLineCount += lineCount;
			tLineOutageCounts[lineCount]++;
		}

        if(useLines){
		    vCurSolution[gens.size()+lines.size()] 	 = stateProb;
		    vCurSolution[gens.size()+lines.size()+1] = totalCap;
        }else{
            vCurSolution[gens.size()] 	 = stateProb;
		    vCurSolution[gens.size()+1] = totalCap;
        }

		timer1.startTimer();

		if(successStates.find(curSolution) != successStates.end()){
			Collisions++;
			continue;
		}
		timer1.stopTimer();
		searchTime += timer1.getElapsedTime();

		classifier->init();
		sol = classifier->run(vCurSolution, excess);
		if(sol != 0.0) { failed = true;}
        else           { failed = false;}

		if(stateProb < epsilon){
			stateProb = epsilon;
			failed = true;
		}
		sampledStateProbs[curSolution] = stateProb;

		if(stateProb*100 > swarm[i].pBestValue){
			swarm[i].pBest      = swarm[i].pos;
			swarm[i].pBestValue = stateProb*100;
		}
		if(stateProb*100 >  gBestValue){
			gBest       = swarm[i].pos;
			gBestValue  = stateProb*100;
		}

		if(failed){
			sampledStates[curSolution] = 0;
			uniqueStates["1" + curSolution] = vCurSolution;
		}else{
			sampledStates[curSolution] = 1;
			uniqueStates["0" + curSolution] = vCurSolution;
		}
	}
}

bool PSO_Sampler::checkConvergence(int j){
	bool retValue = false;
	double totalProbDown = 0, totalProbUp=0;

	numSamples = sampledStates.size();
	std::map<string, int>::iterator i;
	int count = 0;
	for(i=sampledStates.begin(); i!= sampledStates.end(); i++){
		if(i->second == 0) {
			totalProbDown += sampledStateProbs[i->first];
			count ++;
		}else{
			totalProbUp += sampledStateProbs[i->first];
		}
	}
    //std::cout << LOLP << " " << change/j << " " << sigma << std::endl;

	pLOLP   = LOLP;
	LOLP    = totalProbDown;
    vLOLP = (1/numSamples) * (sumXSquared/numSamples - pow(LOLP,2));
    sigma = sqrt((1.0/sampledStates.size()) * ((sampledStates.size()-count)/(double)sampledStates.size() - pow(LOLP,2)));
    sigmas.push_back(sigma);
	NLOLP   = totalProbUp;
	LOLP    = (1-NLOLP+LOLP)/2.0;
	change += fabs(LOLP - pLOLP);

	if(sigma < tolerance){
		retValue = true;
	}
	fStatesCount.push_back(failedStates.size());
	sStatesCount.push_back(successStates.size());

	return retValue;
}
void PSO_Sampler::updatePositions(MTRand& mt){
	double R1, R2, R3, newV;
	double A = 5, B = 3, C = 5;
	for(unsigned int i=0; i< swarm.size(); i++){
		for(int j=0; j<Nd; j++){

			R1 = mt.rand();	R2 = mt.rand(); R3 = mt.rand();

			newV =  swarm[i].vel[j] * W;
			newV += C1 * R1 * (swarm[i].pBest[j] - swarm[i].pos[j]);
			newV += C2 * R2 * (gBest[j] - swarm[i].pos[j]);
			newV = sigMoid(newV);
			swarm[i].vel[j] = newV;

			if(newV > mt.rand()){
				swarm[i].pos[j] = 1;
			}else{
				swarm[i].pos[j] = 0;
			}
		}
	}
}
void PSO_Sampler::run(MTRand& mt){
	bool converged = false;
	int j;

	stateGenerationTime = searchTime = 0;
	tLineOutageCounts.clear(); genOutageCounts.clear();
	tLineOutageCounts.resize(lines.size(), 0); genOutageCounts.resize(gens.size(), 0);
	gBest.resize(gens.size() + lines.size(), 0.0);
	#ifdef _MSC_VER
		gBestValue = -std::numeric_limits<double>::infinity();
	#else
		gBestValue = -INFINITY;
	#endif

	timer.startTimer();

	sumX=0; sumXSquared=0; nSumX=0; nSumXSquared=0;
	curProb 	 = 0.0; vLOLP 		 = 0.0; pLOLP = 0.0;
	numSamples 	 = 1; 	iterations 	 = 0;	sigma = 1.0;
	LOLP 		 = 0.0; pLOLP = 0.0;
	Collisions	 = 0;	avgLineCount = 0;	avgGenCount  = 0;

	uniqueStates.clear(); sampledStateProbs.clear(); sampledStates.clear();

	j=0;
	initPopulation(mt);
    change = 0.0;
	//while(!converged){
	while(j < Nt){
		//cout << j << " ";
		evaluateFitness();
		updatePositions(mt);
		converged = checkConvergence(j);
		converged = false;
		j++;
	}
	totalIterations = j;
	iterations = j;
	timer.stopTimer();
	simulationTime = timer.getElapsedTime();
}

double PSO_Sampler::sigMoid(double v){
	return 1/(1+exp(-v));
}
